// Initial variables
const field = document.querySelector('main');
const cube = document.querySelector('#cube');
const popup = document.querySelector('#popup')
const trees = document.querySelector('#tree')
//let killer = prompt('Please insert your name here: ');
//let escaper = prompt('Please insert your name here: ');
killer = 'Filipe';
escaper = 'CPU';
let speed = 5
let killerWins = 0;
let escaperWins = 0;
let posTop = Math.trunc(Math.random() * 100);
let posLeft = Math.trunc(Math.random() * 100);

// Initialisation of the eventListeners + timeout for player2/cpu winner
setTimeout(escaperScore, 30000)
field.style.cursor = 'crosshair'
document.addEventListener('keydown', testerLogic)
cube.addEventListener('click', killerScore)

function testerLogic(e) {
    //LOGIC
    if (e.keyCode == 38) {
        posTop -= speed;
    }
    if (e.keyCode == 40) {
        posTop += speed;
    }
    if (e.keyCode == 39) {
        posLeft += speed;
    }
    if (e.keyCode == 37) {
        posLeft -= speed;
    }
    //LIMIT
    if (posTop <= -5) {
        posTop = 95;
    } else {
        if (posTop >= 95) {
            posTop = -5;
        } else {
            if (posLeft <= -5) {
                posLeft = 95;
            } else {
                if (posLeft >= 95) {
                    posLeft = -5;
                }
            }
        }
    }
    console.log(e);
    console.log(`Key Pressed: ${e.key}
    posTop: ${posTop}
    posLeft: ${posLeft}`);
    moveCube();
};


// MOVEMENTS HANDLER
function moveCube() { // movement of the cube/duck
    cube.style.left = posLeft + '%';
    cube.style.top = posTop + '%';
}


function listenerKiller() { // name states the function
    document.removeEventListener('keydown', killerScore);
    document.removeEventListener('click', killerScore);
    document.removeEventListener('keydown', escaperScore);
    document.removeEventListener('click', escaperScore);
}


// PLAYER 1 SCORE
function killerScore() {
    killerWins++;
    speed++;
    // Popup to replace `alert`
    popup.textContent = `
    ${killer} Wins! \n
    Already won: ${killerWins}
    `;
    popup.style.display = 'block';
    // Kill Listeners
    listenerKiller;
    if (killerWins >= 10) {
        alert(`${killer} Won!`);
        window.location.reload(false);
    }
    // Difficulty Adjustment
    diffAdj()
    return false;
}


// PLAYER 2 SCORE
function escaperScore() {
    escaperWins++;
    speed--;
    // Popup to replace `alert`
    popup.textContent = `
    ${escaper} Wins! \n
    Already won: ${escaperWins}
    `;
    popup.style.display = 'flex';
    listenerKiller();
    // Kill Listeners
    if (escaperWins >= 10) {
        alert(`${escaper} Won!`);
        window.location.reload(false);
    }
    // reset timeout for continuos scoring
    setTimeout(escaperScore, 30000)
    return false;
}


// PLAY MODE SELECTOR
function mode() {
    let multiplayer = prompt(`Do you want to play in mode multiplayer (m or 2) or you prefer to play in single mode (s or 1)?`)
    for (i of multiplayer) {
        if ('m' == multiplayer || 2 == multiplayer) {
            killer = prompt('Please insert your name here: ');
            escaper = prompt('Please insert your name here: ');
            trees.style.display = 'none';
        }
        if ('s' == multiplayer || 1 == multiplayer) {
            killer = prompt('Please insert your name here: ');
            setInterval(cpuPlayer, 100)
        }
    }
}


// FAKE PLAYER
function cpuPlayer() {
    move = Math.trunc(Math.random() * 100)
    let x = ''
    let y = 0
    if (move >= 0 && move <= 25) {
        x = 'ArrowUp';
        y = 38
    }
    if (move >= 25 && move < 50) {
        x = 'ArrowDown';
        y = 40
    }
    if (move >= 50 && move < 75) {
        x = 'ArrowLeft';
        y = 37
    }
    if (move >= 75 && move < 100) {
        x = 'ArrowRight';
        y = 39
    }
    console.log(move);
    e = new KeyboardEvent('keydown', {
        'isTrusted': true.toString(),
        'key': x,
        'code': x,
        'keyCode': y
    })
    console.log(e);
    document.dispatchEvent(e)
}

function diffAdj() {
    cube.style.width = -25
    cube.style.height = -25
}


mode()
